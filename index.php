<?php

	header('Access-Control-Allow-Origin: http://wtpscriptlibraries.com');

	session_start(); 
	error_reporting(0);

?>


<!DOCTYPE html> 
<html lang="en"> 
	<head> 
		<title>Responsive Template</title> 
		
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		
		<meta name="viewport" content="width=device-width, initial-scale=1"> 

		<link rel="stylesheet" href="css/bootstrap.min.css" />
		<link rel="stylesheet" href="css/local.css" />
		
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug 
		<script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
		-->
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		
	</head> 
<body> 

	<div class="navbar navbar-inverse navbar-fixed-top top-nav" role="navagation" style="min-width:310px">
		<div class="center-block bot-line"> <img src='images/logo.gif' class="img-responsive center-block" alt="Responsive image" /> </div>
		<div class="container-fluid">
				<?php include("includes/menu_navbar.php"); ?>
		</div><!-- /container -->
	</div>

	<div class="container-fluid"  style="min-width:320px">
		<div class="center-block" style="margin-top:10px;overflow:hidden;text-align:center;" > 
			<img src='images/brazil.jpg' class="img-responsive center-block" alt="Responsive image" style="min-width:480px;"/> 
			<div id="search" style="background:#999;">	
				<form class="navbar-form" role="search" style="margin:5px;">
					<div class="input-group">
						<input type="text" class="form-control" placeholder="Search">
						<div class="input-group-addon"><span class="glyphicon glyphicon-search"> </span></div>
					</div>
				</form>
			</div>

			<div id="ggle" style="text-align:center;">
				<span>Change Website Language &nbsp; </span>
				<span id="google_translate_element"></span><script>
				function googleTranslateElementInit() {
				  new google.translate.TranslateElement({
					pageLanguage: 'en',
				//	includedLanguages: 'af,sq,ar,be,bg,ca,zh-CN,zh-TW,hr,cs,da,nl,en,eo,et,tl,fi,fr,gl,de,el,ht,iw,hi,hu,is,id,ga,it,ja,ko,lv,lt,mk,ms,mt,no,fa,pl,pt,ro,ru,sr,sk,sl,es,sw,sv,th,tr,uk,vi,cy,yi',
					layout: google.translate.TranslateElement.InlineLayout.HORIZONTAL
				  }, 'google_translate_element');
				}
				</script>
				<script src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
			</div>
			
			
		</div>

		<div class="row">

			<div class="col-md-3 sidebar-offcanvas" id="sidebar" role="navigation">
				<?php include("includes/menu_countryNodes.php"); ?>
			</div><!--/span-->		
			
			<div class="col-md-9">
				<div class="row">
					<div class="col-md-12" style="background: #fff">
						<?php include("includes/temp_content_placeholder1.php"); ?>
					</div><!--/span-->
				</div><!--/row-->

			</div><!--/span-->
		</div><!--/row-->

		<hr>

		<footer>
		<p>&copy; World Trade Press 2014</p><span class="sampleClass"></span>
		</footer>	

	</div><!--/.fluid-container-->

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
	<script type="text/javascript" src="http://www.wtpscriptlibraries.com/jquery/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/local.js"></script>

</body>
</html>