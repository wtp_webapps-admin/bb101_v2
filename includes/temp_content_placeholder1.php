	<h1>Brazil: Business Culture<br> Business Meetings</h1>
	<div class="picright">
		<span><a title="Brazilian business is dominated by white males.&lt;br &gt;&lt;/a&gt;Photograph by: Vitor Pamplona&lt;br /&gt;License: Creative Commons Attribution 2.0" target="_blank" rel="lightbox" href="http://www.globalroadwarrior.com/GRWGraphics/brazil/BRA_BS_meeting.jpg"><img border="0" title="" src="http://www.globalroadwarrior.com/GRWGraphics/brazil/T_BRA_BS_meeting.jpg" alt=""><br></a>Brazilian business is dominated by white males.</span>
	</div>
	<p>
		When attending a business meeting, dress to impress and wear good shoes. Patience and a composed demeanor are also a plus. Brazilian businesspeople do not like to jump right in to business discussions, so make sure you start with small talk and do not try to push the agenda forward. However, your business proposal should be presented in a straightforward and pleasant manner. Brazilian businesspeople appreciate the obvious appearance of preparedness. When you are in Brazil, make sure to allow yourself enough time between meetings, and try not to cram too many appointments into one day.</p>
	<p>
		According to research by Berlin-based Transparency International, Brazil is one of the 15 most corrupt nations in the world, from government agencies to the general populace. The ongoing inequity among groups perpetuates a class system, social discrimination on the basis of skin color persists, and there is disparity of opportunity between men and women.</p>
	<p>
		Nonetheless, Brazil is full of potential for the businessperson who can navigate the culture.</p>
	<h5>
		<strong>Preparation</strong></h5>
	<p>
		Although Brazilians are tolerant of differences, you will discover that the office environment is a white male’s world. Few people of color or women have achieved executive status, academic titles, or government positions. Brazilian companies are the worst culprits in such discrimination.</p>
	<p>
		Foreign businesswomen in leadership roles will need to make their position and status clear to their counterparts to ensure the smooth flow of business. They should also expect compliments (which is considered polite and not harassment), but be proactive in preventing unwanted attention.</p>
	<p>
		Brazilians are proud to be the only Portuguese-speaking country in Latin America and will be insulted by foreigners speaking Spanish to them. The other side of this is that they will appreciate any attempt you make at speaking their language. The following are a few key words and phrases:</p>
	<table cellspacing="0" cellpadding="0" border="0" class="members">
		<tbody>
			<tr>
				<td style=" width: 20px; height: 8px;">
					&nbsp;</td>
				<td style=" vertical-align: top; width: 156px; height: 9px;">
					&nbsp;</td>
				<td style=" vertical-align: top; width: 440px; height: 9px;">
					&nbsp;</td>
			</tr>
			<tr>
				<td style=" width: 20px; height: 54px;">
					&nbsp;</td>
				<td style=" vertical-align: top; width: 156px; height: 54px;">
					<p>
						Hello</p>
				</td>
				<td style=" vertical-align: top; width: 440px; height: 54px;">
					<p>
						<em>Muito prazer</em> (my pleasure) can be used upon first meeting someone.<br>
						<em>Como vai</em> and <em>tudo bem </em>are used once you are more familiar with someone and can show you are making an effort to know them.</p>
				</td>
			</tr>
			<tr>
				<td style=" width: 20px; height: 48px;">
					&nbsp;</td>
				<td style=" vertical-align: top; width: 156px; height: 48px;">
					<p>
						How are you?</p>
				</td>
				<td style=" vertical-align: top; width: 440px; height: 48px;">
					<p>
						<em>Como você está?</em> (formal)<br>
						<em>Como vai? </em>(informal)</p>
				</td>
			</tr>
			<tr>
				<td style=" width: 20px; height: 24px;">
					&nbsp;</td>
				<td style=" vertical-align: top; width: 156px; height: 24px;">
					<p>
						Bye/See you later</p>
				</td>
				<td style=" vertical-align: top; width: 440px; height: 24px;">
					<p>
						<em>Tchau</em></p>
				</td>
			</tr>
			<tr>
				<td style=" width: 20px; height: 32px;">
					&nbsp;</td>
				<td style=" vertical-align: top; width: 156px; height: 32px;">
					<p>
						Good morning</p>
				</td>
				<td style=" vertical-align: top; width: 440px; height: 32px;">
					<p>
						<em>Bom dia </em>(Note that <em>dia</em> is pronounced with a soft "d," similar to the French "j" in <em>jour</em>.)</p>
				</td>
			</tr>
			<tr>
				<td style=" height: 24px;">
					&nbsp;</td>
				<td style=" vertical-align: top; width: 156px; height: 24px;">
					<p>
						Good day/afternoon</p>
				</td>
				<td style=" vertical-align: top; width: 440px; height: 24px;">
					<p>
						<em>Boa tarde</em></p>
				</td>
			</tr>
			<tr>
				<td style=" height: 24px;">
					&nbsp;</td>
				<td style=" vertical-align: top; width: 156px; height: 24px;">
					<p>
						Good evening</p>
				</td>
				<td style=" vertical-align: top; width: 440px; height: 24px;">
					<p>
						<em>Boa noite</em></p>
				</td>
			</tr>
			<tr>
				<td style=" height: 24px;">
					&nbsp;</td>
				<td style=" vertical-align: top; width: 156px; height: 24px;">
					<p>
						Thank you</p>
				</td>
				<td style=" vertical-align: top; width: 440px; height: 24px;">
					<p>
						<em>Obrigado </em>(for men), <em>Obrigada </em>(for women)</p>
				</td>
			</tr>
			<tr>
				<td style=" width: 20px; height: 24px;">
					&nbsp;</td>
				<td style=" vertical-align: top; width: 156px; height: 24px;">
					<p>
						You’re welcome</p>
				</td>
				<td style=" vertical-align: top; width: 440px; height: 24px;">
					<p>
						<em>De nada</em></p>
				</td>
			</tr>
			<tr>
				<td style=" width: 20px; height: 8px;">
					&nbsp;</td>
				<td style=" vertical-align: top; width: 156px; height: 9px;">
					&nbsp;</td>
				<td style=" vertical-align: top; width: 440px; height: 9px;">
					&nbsp;</td>
			</tr>
		</tbody>
	</table>
	<h5>
		<strong>Scheduling</strong></h5>
	<p>
		Scheduling an appointment will be necessary and can be done on short notice. Try to do it at least two to three weeks in advance and then confirm the appointment in writing. Meetings can be scheduled by your secretary with their secretary, or by two executives. Be aware that canceling or changing the time at the last minute is not uncommon. It is important to arrive on time to appointments in Sao Paulo and Brasilia. However, for meetings in Rio de Janeiro, Salvador, and other cities, being a few minutes late is acceptable.</p>
	<p>
		Don’t arrange too many appointments for one day or make your schedule too tight. Events are bound to rise that will cause delays, such as being invited to an impromptu lunch with your counterparts, during which the opportunity to discuss important business matters can arise.</p>
	<p>
		Brazilians view time as something beyond their control, and will value relationships above schedules. Spontaneous and fun loving, they have a relaxed attitude toward appointments and schedules. Even if you find this attitude frivolous, don’t show impatience if you are kept waiting.</p>
	<p>
		Cold calling is an acceptable approach to making an initial contact with a Brazilian company, and lists of Brazilian companies are available at consulates and embassies around the world, as well as on the Internet or through big firms such as Price Waterhouse.</p>
	<p>
		Having your initial meeting, by your invitation, over drinks or a business breakfast is a good way to initiate a partnership. The informal tone will carry over into further meetings.</p>
	<p>
		A <em>despachante</em> (local business intermediary) is highly recommended when undertaking business in Brazil. A good <em>despachante </em>can leverage his relationships and help make initial contact with desired individuals, bridge the gap between cultures, and even get your applications placed first on the pile. Find good <em>despachantes</em> by approaching your embassy, a trade organization, chamber of commerce, or a local legal or accounting firm.</p>
	<h5>
		<strong>Business Attire</strong></h5>
	<p>
		Appearance counts in Brazil; it will reflect upon you and your company, and you should therefore look smart. Corporate people wear business suits: executives wear three-piece suits and office workers wear two-piece suits. In warmer and more rural locations you may not need to wear a tie. Shorts are for the beach and are taboo in business environments, despite sweltering summers. Women can wear more casual dresses. Brazilian women are quite flamboyant in their dress, even in business environments. Pay particular attention to your shoes, which should be stylish and well kept; Brazil is an important manufacturer of footwear (as well as a fashion-conscious culture) and they take shoes seriously.</p>
	<p>
		Dressing casually during social occasions is acceptable, even though women will dress up more than men for an evening out.</p>
	<h5>
		<strong>Meeting Protocol</strong></h5>
	<p>
		Brazilians will appreciate a chance to make advance preparations, so send along your company history, proposal outline, list of attendees and their titles, short biographies, and other relevant material. Providing this material in Portuguese will be especially welcomed.</p>
	<p>
		Your hosts may extend the courtesy of providing a driver to meet you at the airport and take you to your hotel or to the company. A receptionist will meet you upon your arrival at the company and will show you to the meeting room. Your counterpart’s assistant may also meet you. While waiting for the boss to arrive, use this opportunity to break the ice and make discreet inquiries into the company’s hierarchy. Traditionally, and in most cases, the oldest person is also the most senior. However, new enterprises in fields such as finance are seeing younger individuals, trained overseas, take up senior roles.</p>
	<p>
		Having one of your senior executives attend an initial meeting will help with the initial presentation of your company; this may also be the approach of the Brazilian side.</p>
	<h6>
		<strong>Entering the Meeting Room: Hierarchy</strong></h6>
	<p>
		It is normal to greet and be introduced to everyone present in the meeting room.</p>
	<p>
		While not always immediately apparent, hierarchy is strong in Brazilian companies, especially in family-run operations. The general rule is that each stratum in the hierarchy generally keeps to itself. Only on rare occasions will executives and managers mix with their underlings, and if they do so it will be for very clear reasons.</p>
	<h6>
		<strong>Introductions</strong></h6>
	<p>
		Shaking hands with everyone upon arrival and before departure is crucial.</p>
	<p>
		Men shake hands with men and maintain constant eye contact during the introductions, while women kiss each other once on each cheek. A man being introduced to a woman must wait for her to initiate a handshake.</p>
	<p>
		Hugging and backslapping are common between male Brazilians.</p>
	<h6>
		<strong>Forms of Address</strong></h6>
	<p>
		Names are given in the order of first name and then surname. Initially, be sure to include their title <em>Senhor</em> (Mr.) or <em>Senhora</em> (Mrs.) or <em>Dona </em>(Ms.) followed by the first name, during any introduction and discourse. Anyone with a college degree will carry the title <em>Doutor </em>or <em>Doutora</em> (Doctor or Professor), depending on their level. You will probably move to a first-name basis quite quickly; nevertheless, wait for your Brazilian counterparts to initiate this step.</p>
	<p>
		Company chief executives are usually called <em>Presidente, w</em>hile managing directors and vice presidents are titled <em>Diretor superintendente</em>.</p>
	<h6>
		<strong>Business Cards</strong></h6>
	<p>
		Exchanging business cards is essential during initial meetings. Brazilians often bend the top corner of their business card before handing it to you, as gesture to personalize the exchange.</p>
	<p>
		Have one side of your card translated into Portuguese and include graduate degrees and professional titles. Present your card with the Portuguese side facing upward, a smile, and eye contact, and take some time to examine any card you receive in return.</p>
	<h6>
		<strong>Body Language</strong></h6>
	<p>
		Brazilian body language is animated and extensive. In fact, body language in Brazil not only enhances verbal communication; it can sometimes even replace it.</p>
	<p>
		Two particularly obscene gestures in Brazil include slapping the fist into the palm, which carries sexual connotations, and the "OK" sign (a circle with your forefinger and thumb), which is especially rude and insulting. The thumb-up is a better gesture to use to indicate everything is OK.</p>
	<p>
		Additional popular gestures include pulling at one’s earlobes to indicate that something, be it wine, music, etc., is very good. Tapping your thumb against your fingertips means that something is full of people, such as a restaurant or bus. Polishing your nails against your chest is a warning, as in "watch out."</p>
	<p>
		Physical contact is an everyday part of communications in Brazil. This includes touching arms, elbows, and backs. Brazilians also stand closer to one another than many North Americans and Northern Europeans are used to during conversation. Don’t back away even if you feel your personal space invaded.</p>
	<p>
		Wiping your hands together is a sign that "it doesn’t matter" and clicking your tongue or shaking your head is a sign of disapproval or disagreement.</p>
	<h5>
		<strong>Meeting Starters: Small Talk versus "Getting Down to Business"</strong></h5>
	<p>
		Meetings will always start with small talk. Brazilians dislike those who jump straight to business. Expect to be offered a <em>cafezinho</em> (espresso), a glass of water, and/or fresh juice first. Small talk may actually be quite extensive and include questions on various topics, excluding one’s private life, which is not appropriate for discussion during meetings. Humor is appreciated, but make sure to keep it light and friendly.</p>
	<p>
		Initial meetings will be aimed at building rapport between the two parties, so be patient and use this time to get to know your counterpart and vice versa.</p>
	<p>
		Business discussions may begin during this time, but pushing your agenda is unrealistic, as is expecting first meetings to lead to decision-making and implementation.</p>
	<p>
		Sticking around once the meeting is finished is good protocol and an opportunity to engage in additional small talk. Leaving immediately is a sign you have better things to do, and this can be construed negatively toward you.</p>
	<p>
		Good topics to talk about include <em>futbol </em>(soccer), Brazil’s beautiful beaches, and the country’s growth. Avoid topics such as politics, poverty, religion, Argentina (Brazil’s rival), and deforestation. Steer clear of private subjects such as age, salary, and job status.</p>
	<h5>
		<strong>Conducting the Meeting</strong></h5>
	<p>
		Presenting your business proposal should be done in a straightforward and pleasant manner. Any presentation materials must be attractive and contain clear visuals. While many Brazilians have some grasp of English, it is good practice to have any materials and handouts translated to Portuguese. This will be appreciated and will help you get your message across.</p>
	<p>
		While all comments should be addressed to the whole group, be sure to direct some special attention to the highest-ranking person in the group, who will also be the decision maker. Answer all questions openly, and never appear defensive or superior to anyone in the group.</p>
	<p>
		Be aware that Brazilians denote thousands using a period and decimals using a comma. Therefore, $1,000.00 is written $1.000,00.</p>
	<h5>
		<strong>Decision Making</strong></h5>
	<p>
		Brazilian companies are no exception to South American hierarchical organization. Key decisions are made by the few holding the most senior positions within the structure. You must quickly develop an understanding of your counterpart’s corporate structure in order to avoid wasting time and resources dealing with individuals who don’t have the authority to make a final decision. Understanding the subtleties of these internal relationships and using them to your benefit may require the skill of a <em>despachante</em>.</p>
	<p>
		The decision-making structure can also be multileveled and include several people. Proposals are pushed upward through channels to senior managers before a decision is reached, but input from those other than the final decision makers will be seriously considered.</p>
	<p>
		Don’t rush the decision-making process. Instead, give your counterparts as much time as they need to discuss it.</p>
	<h5>
		<strong>Gifts</strong></h5>
	<p>
		Brazilians are generous. They will give gifts on all special occasions. Bring a gift when you’re invited to someone’s home. On such an occasion flowers or wine will do. If you bring a gift to a first meeting, make sure it is unique, and that it cannot be acquired in Brazil. If in doubt, it is better not to bring anything. On subsequent meetings, and when you know your counterpart better, it may be appropriate to bring something he likes, such as a book or specialty item that isn’t available in Brazil.</p>
	<p>
		Alcoholic beverages such as Scotch were once very expensive and therefore made an impressive gift. Today, however, such a gift would not be appreciated in Brazil.</p>
	<h5>
		<strong>Follow-up</strong></h5>
	<p>
		Following up with Brazilians is good business protocol. Before the end of your meeting, make sure to secure your next appointment to ensure there will be a follow-up on all your efforts. Often the highest-level person at the meeting will delegate execution of the details to subordinates. Find out who are the executors, how they want to be contacted, what needs to be done, and when.</p>
	<p>
		Latin Americans tend to communicate orally rather than through the written word. Therefore, always follow up any written document with a phone call or a visit.</p>
