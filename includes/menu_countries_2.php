<?php
error_reporting(0);
?>

<div class="nav-collapse collapse" id="menuCountry">
	
	[country name]

	<div class="accordion-group">
		<div class="accordion-heading">
			<a class="accordion-toggle" data-toggle="collapse" data-parent="#menuCountry" href="#overviews">
				Overviews
			</a>
		 </div>
		<div id="overviews" class="accordion-body collapse">
			<div class="accordion-inner">
				<a href="content.php?cat=grw&amp;nid=66&amp;next_nid=67&amp;cid=9&amp;parent=Overviews">Trade Overview</a>
				<a href="content.php?cat=grw&amp;nid=67&amp;next_nid=68&amp;cid=9&amp;parent=Overviews">Business Overview</a>
				<a href="content.php?cat=grw&amp;nid=65&amp;next_nid=66&amp;cid=9&amp;parent=Overviews">Country Snapshot</a>
				<a href="country_facts.php?cid=9&amp;parent=Overview">Country Facts</a>
			</div>
		</div>
	</div>
	
	<div class="accordion-group">
		<div class="accordion-heading">
			<a class="accordion-toggle" data-toggle="collapse" data-parent="#menuCountry" href="#bizCulture">
				Business Culture
			</a>
		 </div>
		<div id="bizCulture" class="accordion-body collapse">
			<div class="accordion-inner"> 
				<a href="content.php?cat=grw&amp;nid=13.12&amp;next_nid=13.13&amp;cid=9&amp;parent=Business Culture">The Business Experience</a>
				<a href="content.php?cat=grw&amp;nid=13.04&amp;next_nid=13.05&amp;cid=9&amp;parent=Business Culture">Decision Making</a>
				<a href="content.php?cat=grw&amp;nid=13.11&amp;next_nid=13.12&amp;cid=9&amp;parent=Business Culture">Meetings</a>
				<a href="content.php?cat=grw&amp;nid=13.10&amp;next_nid=13.11&amp;cid=9&amp;parent=Business Culture">Negotiating</a>
				<a href="content.php?cat=grw&amp;nid=13.06&amp;next_nid=13.07&amp;cid=9&amp;parent=Business Culture">Entertaining</a>
				<a href="content.php?cat=grw&amp;nid=13.08&amp;next_nid=13.09&amp;cid=9&amp;parent=Business Culture">Attire</a>
				<a href="content.php?cat=grw&amp;nid=13.13&amp;next_nid=13.14&amp;cid=9&amp;parent=Business Culture">Businesswomen</a>
				<a href="content.php?cat=grw&amp;nid=1.05&amp;next_nid=1.06&amp;cid=9&amp;parent=Business Culture">Business Workweek</a>
				<a href="content.php?cat=grw&amp;nid=20.15&amp;next_nid=20.16&amp;cid=9&amp;parent=Business Culture">Gift Giving</a>			
				<a href="content.php?cat=grw&amp;nid=13.02&amp;next_nid=13.03&amp;cid=9&amp;parent=Business Culture">Greetings and Courtesies</a>
				<a href="content.php?cat=grw&amp;nid=20.17&amp;next_nid=20.18&amp;cid=9&amp;parent=Business Culture">Holidays and Festivals</a>				
				<a href="content.php?cat=grw&amp;nid=20.08&amp;next_nid=20.09&amp;cid=9&amp;parent=Business Culture">Stereotypes</a>
				<a href="content.php?cat=grw&amp;nid=20.31&amp;next_nid=20.32&amp;cid=9&amp;parent=Business Culture">Time Orientation</a>
				<a href="content.php?cat=grw&amp;nid=20.33&amp;next_nid=20.34&amp;cid=9&amp;parent=Business Culture">Women in Business</a>
				<a href="content.php?cat=grw&amp;nid=20.32&amp;next_nid=20.33&amp;cid=9&amp;parent=Business Culture">Women in Culture</a>	
			</div>
		</div>
	</div>
	
	
	
	
	

	<div class="accordion-group">
		<div class="accordion-heading">
			<a class="accordion-toggle" data-toggle="collapse" data-parent="#menuCountry" href="#bizFormation">
				Business Formation
			</a>
		 </div>
		<div id="bizFormation" class="accordion-body collapse">
			<div class="accordion-inner"> 
				<a href="content.php?cat=grw&amp;nid=68&amp;next_nid=68.01&amp;cid=9&amp;parent=Business Formation">Starting a Business</a>
			</div>
		</div>
	</div>
	
	
	
	
	
	<div class="accordion-group">
		<div class="accordion-heading">
			<a class="accordion-toggle" data-toggle="collapse" data-parent="#menuCountry" href="#BusinessTravel">
				Business Travel
			</a>
		 </div>
		<div id="BusinessTravel" class="accordion-body collapse">
			<div class="accordion-inner">	 
				<a href="content.php?cat=grw&amp;nid=63&amp;next_nid=64&amp;cid=9&amp;parent=Travel">Climate</a>
				<a href="content.php?cat=grw&amp;nid=4.03&amp;next_nid=4.04&amp;cid=9&amp;parent=Travel">Customs Entry Travelers</a>
				<a href="content.php?cat=grw&amp;nid=embassies&amp;cid=9&amp;parent=Travel">Embassies and Consulates</a>
				<a href="content.php?cat=grw&amp;nid=9.04&amp;next_nid=9.05&amp;cid=9&amp;parent=Travel">Emergency Numbers</a><a href="http://reservations.atozworldtrade.com/templates/379121/hotels/list?destination=Baku+Azerbaijan&amp;filter.starRates=5&amp;filter.starRates=4&amp;filter.starRates=3&amp;filter.starRates=2&amp;filter.sortedBy=traveler_hl&amp;filtering=true">Hotels</a>
				<a href="content.php?cat=grw&amp;nid=4.05&amp;next_nid=4.05001&amp;cid=9&amp;parent=Travel">Tipping</a>
				<a href="content.php?cat=grw&amp;nid=4.01&amp;next_nid=4.02&amp;cid=9&amp;parent=Travel">Visa and Passport</a>
				<a href="weather.php?cid=9&amp;parent=Travel">Weather Forecast</a>
				<a href="content.php?cat=grw&amp;nid=7.05&amp;next_nid=7.06&amp;cid=9&amp;parent=Business Travel">Disease Risks and Prevention</a>
				<a href="content.php?cat=grw&amp;nid=7.07&amp;next_nid=7.08&amp;cid=9&amp;parent=Business Travel">Health Advisories</a>
				<a href="content.php?cat=grw&amp;nid=7.06&amp;next_nid=7.07&amp;cid=9&amp;parent=Business Travel">Health Care System</a>
				<a href="content.php?cat=grw&amp;nid=4.02&amp;next_nid=4.03&amp;cid=9&amp;parent=Business Travel">Immunization</a>
				<a href="content.php?cat=grw&amp;nid=7.08&amp;next_nid=7.09&amp;cid=9&amp;parent=Business Travel">Insurance and Med-Evac</a>
				<a href="content.php?cat=grw&amp;nid=7.02&amp;next_nid=7.03&amp;cid=9&amp;parent=Business Travel">Directory of Health Services</a>
			</div>
		</div>
	</div>
	
	
	
	
	
	<div class="accordion-group">
		<div class="accordion-heading">
			<a class="accordion-toggle" data-toggle="collapse" data-parent="#menuCountry" href="#Communications">
				Communications
			</a>
		 </div>
		<div id="Communications" class="accordion-body collapse">
			<div class="accordion-inner">
				<a href="content.php?cat=grw&amp;nid=9.02&amp;next_nid=9.03&amp;cid=9&amp;parent=Communications">Dialing Guide</a>
				<a href="content.php?cat=grw&amp;nid=9.04&amp;next_nid=9.05&amp;cid=9&amp;parent=Communications">Emergency Numbers</a>
				<a href="content.php?cat=grw&amp;nid=9.07&amp;next_nid=9.08&amp;cid=9&amp;parent=Communications">Cell Phone Dialing Guide</a>
				<a href="content.php?cat=grw&amp;nid=11.02&amp;next_nid=11.03&amp;cid=9&amp;parent=Communications">Internet Access</a>
				<a href="content.php?cat=grw&amp;nid=9.18&amp;next_nid=9.19&amp;cid=9&amp;parent=Communications">Postal Service</a>
				<a href="content.php?cat=grw&amp;nid=9.13&amp;next_nid=9.14&amp;cid=9&amp;parent=Communications">Public Phones and Calling Cards</a>
			</div>
		</div>
	</div>
	
	
	
	
	
	
	<div class="accordion-group">
		<div class="accordion-heading">
			<a class="accordion-toggle" data-toggle="collapse" data-parent="#menuCountry" href="#CountryProfile">
				Country Profile
			</a>
		 </div>
		<div id="CountryProfile" class="accordion-body collapse">
			<div class="accordion-inner"> 
				<a href="content.php?cat=grw&amp;nid=1.03&amp;next_nid=1.04&amp;cid=9&amp;parent=Demographics">Demographics</a>	 		
				<a href="content.php?cat=grw&amp;nid=1.02&amp;next_nid=1.03&amp;cid=9&amp;parent=Geography">Geography</a>
				<a href="content.php?cat=grw&amp;nid=1.07&amp;next_nid=1.08&amp;cid=9&amp;parent=Country Profile">Government</a>
				<a href="content.php?cat=grw&amp;nid=1.08&amp;next_nid=1.09&amp;cid=9&amp;parent=Country Profile">Government Leaders</a>
				<a href="content.php?cat=grw&amp;nid=1.04&amp;next_nid=1.05&amp;cid=9&amp;parent=Country Profile">Economy and Trade</a>
				<a href="content.php?cat=grw&amp;nid=1.01&amp;next_nid=1.02&amp;cid=9&amp;parent=Country Profile">People</a>
			</div>
		</div>
	</div>
	
	
	
	
	
	
	<div class="accordion-group">
		<div class="accordion-heading">
			<a class="accordion-toggle" data-toggle="collapse" data-parent="#menuCountry" href="#Export">
				Export
			</a>
		 </div>
		<div id="Export" class="accordion-body collapse">
			<div class="accordion-inner"> 
				<a href="content.php?cat=grw&amp;nid=61.09&amp;next_nid=61.10&amp;cid=9&amp;parent=Export">Basic Process</a>
				<a href="content.php?cat=grw&amp;nid=61.10&amp;next_nid=61.11&amp;cid=9&amp;parent=Export">Documents</a>
				<a href="content.php?cat=grw&amp;nid=61.11&amp;next_nid=61.12&amp;cid=9&amp;parent=Export">Restricted and Prohibited</a>
				<a href="content.php?cat=grw&amp;nid=61.12&amp;next_nid=61.13&amp;cid=9&amp;parent=Export">Special Provisions</a>
				<a href="content.php?cat=grw&amp;nid=61.18&amp;next_nid=61.19&amp;cid=9&amp;parent=Export">Trade Agreements</a>			
				<a href="content.php?cat=grw&amp;nid=61.14&amp;next_nid=61.15&amp;cid=9&amp;parent=Export">Contacts</a>
			</div>
		</div>
	</div>
	
	
	
	
	
	<div class="accordion-group">
		<div class="accordion-heading">
			<a class="accordion-toggle" data-toggle="collapse" data-parent="#menuCountry" href="#Import">
				Import
			</a>
		 </div>
		<div id="Import" class="accordion-body collapse">
			<div class="accordion-inner"> 
				<a href="content.php?cat=grw&amp;nid=61.02&amp;next_nid=61.03&amp;cid=9&amp;parent=Import">Basic Process</a>
				<a href="content.php?cat=grw&amp;nid=61.03&amp;next_nid=61.04&amp;cid=9&amp;parent=Import">Documents</a>
				<a href="content.php?cat=grw&amp;nid=61.04&amp;next_nid=61.05&amp;cid=9&amp;parent=Import">Duties and Taxes</a>
				<a href="content.php?cat=grw&amp;nid=61.05&amp;next_nid=61.06&amp;cid=9&amp;parent=Import">Registration Requirements</a>
				<a href="content.php?cat=grw&amp;nid=61.06&amp;next_nid=61.07&amp;cid=9&amp;parent=Import">Tariff Classification</a>
				<a href="content.php?cat=grw&amp;nid=61.07&amp;next_nid=61.08&amp;cid=9&amp;parent=Import">Restricted and Prohibited</a>
				<a href="content.php?cat=grw&amp;nid=61.08&amp;next_nid=61.09&amp;cid=9&amp;parent=Import">Special Provisions</a>
				<a href="content.php?cat=grw&amp;nid=61.15&amp;next_nid=61.16&amp;cid=9&amp;parent=Import">Non-Tariff Barriers</a>
				<a href="content.php?cat=grw&amp;nid=61.16&amp;next_nid=61.17&amp;cid=9&amp;parent=Import">Standards, Testing, etc.</a>
				<a href="content.php?cat=grw&amp;nid=61.18&amp;next_nid=61.19&amp;cid=9&amp;parent=Import">Trade Agreements</a>			
				<a href="content.php?cat=grw&amp;nid=61.13&amp;next_nid=61.14&amp;cid=9&amp;parent=Import">Contacts</a>			
			</div>
		</div>
	</div>
	
	
	
	
	<div class="accordion-group">
		<div class="accordion-heading">
			<a class="accordion-toggle" data-toggle="collapse" data-parent="#menuCountry" href="#InvestmentClimate">
				Investment Climate
			</a>
		 </div>
		<div id="InvestmentClimate" class="accordion-body collapse">
			<div class="accordion-inner"> 
				<a href="content.php?cat=grw&amp;nid=70.01&amp;next_nid=70.02&amp;cid=9&amp;parent=Investment Climate">Investment Climate</a>
			</div>
		</div>
	</div>
	
	
	
	
	<div class="accordion-group">
		<div class="accordion-heading">
			<a class="accordion-toggle" data-toggle="collapse" data-parent="#menuCountry" href="#Language">
				Language
			</a>
		 </div>
		<div id="Language" class="accordion-body collapse">
			<div class="accordion-inner">
				<a href="language.php?ctry=Azerbaijan&amp;topic=Terms">Essential Terms</a>
				<a href="language.php?ctry=Azerbaijan&amp;topic=s_Accounting">Accounting</a>			
				<a href="language.php?ctry=Azerbaijan&amp;topic=Advertising">Advertising</a>
				<a href="language.php?ctry=Azerbaijan&amp;topic=Computers">Computers</a>
				<a href="language.php?ctry=Azerbaijan&amp;topic=Contracts">Contracts</a>
				<a href="language.php?ctry=Azerbaijan&amp;topic=Countries_and_Capitals">Countries and Capitals</a>
				<a href="language.php?ctry=Azerbaijan&amp;topic=Internet">Internet</a>
				<a href="language.php?ctry=Azerbaijan&amp;topic=Legal_System">Legal System</a>
				<a href="language.php?ctry=Azerbaijan&amp;topic=Marketing">Marketing</a>
				<a href="language.php?ctry=Azerbaijan&amp;topic=Numbers">Numbers</a>
				<a href="language.php?ctry=Azerbaijan&amp;topic=s_Professions">Professions</a>
				<a href="language.php?ctry=Azerbaijan&amp;topic=General_Travel">Travel General</a>
				<a href="language.php?ctry=Azerbaijan&amp;topic=Travel_Items">Travel Items</a>
				<a href="language.php?ctry=Azerbaijan&amp;topic=Travel_Services">Travel Services</a>
			</div>
		</div>
	</div>




	
	<div class="accordion-group">
		<div class="accordion-heading">
			<a class="accordion-toggle" data-toggle="collapse" data-parent="#menuCountry" href="#Maps">
				Maps
			</a>
		 </div>
		<div id="Maps" class="accordion-body collapse">
			<div class="accordion-inner">	 
				<a href="maps.php?cid=9">9 Maps</a>	 		
			</div>
		</div>
	</div>
	
	
	
	
	<div class="accordion-group">
		<div class="accordion-heading">
			<a class="accordion-toggle" data-toggle="collapse" data-parent="#menuCountry" href="#MoneyBanking">
				Money and Banking
			</a>
		 </div>
		<div id="MoneyBanking" class="accordion-body collapse">
			<div class="accordion-inner">	 
				<a href="content.php?cat=grw&amp;nid=3.01&amp;next_nid=3.02&amp;cid=9&amp;parent=Money and Banking">Currency Overview</a>
				<a href="content.php?cat=grw&amp;nid=3.02&amp;next_nid=3.03&amp;cid=9&amp;parent=Money and Banking">Banknote Images</a>
				<a href="content.php?cat=grw&amp;nid=3.08&amp;next_nid=3.09&amp;cid=9&amp;parent=Money and Banking">Coin Images</a>
				<a href="javascript:popUp('http://www.globalroadwarrior.com/currency_converter_php/currency.php?cid=9');">Currency Converter</a>
				<a href="content.php?cat=grw&amp;nid=3.06&amp;next_nid=3.07&amp;cid=9&amp;parent=Money and Banking">Major Banks</a>
			</div>
		</div>
	</div>
	
	
	
	
	
	<div class="accordion-group">
		<div class="accordion-heading">
			<a class="accordion-toggle" data-toggle="collapse" data-parent="#menuCountry" href="#NewsFeeds">
				News Feeds
			</a>
		 </div>
		<div id="NewsFeeds" class="accordion-body collapse">
			<div class="accordion-inner"> 
				<a href="news.php?feed=1&amp;parent=News Feeds">Top Stories</a>
				<a href="news.php?feed=8&amp;parent=News Feeds">Agriculture</a>
				<a href="news.php?feed=5&amp;parent=News Feeds">Banking</a>
				<a href="news.php?feed=2&amp;parent=News Feeds">Business</a>
				<a href="news.php?feed=10&amp;parent=News Feeds">Crime</a>
				<a href="news.php?feed=3&amp;parent=News Feeds">Export</a>
				<a href="news.php?feed=6&amp;parent=News Feeds">Finance</a>
				<a href="news.php?feed=4&amp;parent=News Feeds">Import</a>
				<a href="news.php?feed=7&amp;parent=News Feeds">Trade</a>
			</div>
		</div>
	</div>
	
	
	
	<div class="accordion-group">
		<div class="accordion-heading">
			<a class="accordion-toggle" data-toggle="collapse" data-parent="#menuCountry" href="#Seaports">
				Seaports
			</a>
		 </div>
		<div id="Seaports" class="accordion-body collapse">
			<div class="accordion-inner">
				<a href="seaports_landing.php?cat=grw&amp;nid=13.10&amp;next_nid=13.11&amp;cid=9&amp;parent=Country Profile">Seaports</a>		
			</div>
		</div>
	</div>
	

	
	<div class="accordion-group">
		<div class="accordion-heading">
			<a class="accordion-toggle" data-toggle="collapse" data-parent="#menuCountry" href="#SecurityBriefing">
				Security Briefing
			</a>
		 </div>
		<div id="SecurityBriefing" class="accordion-body collapse">
			<div class="accordion-inner">
				<a href="content.php?cat=grw&amp;nid=6.08&amp;next_nid=6.09&amp;cid=9&amp;parent=Security Briefing">Security Assessment</a>
				<a href="content.php?cat=grw&amp;nid=6.09&amp;next_nid=6.10&amp;cid=9&amp;parent=Security Briefing">Travel Warnings</a>
				<a href="content.php?cat=grw&amp;nid=6.1&amp;next_nid=6.11&amp;cid=9&amp;parent=Security Briefing">Threats to Safety and Security</a>
				<a href="content.php?cat=grw&amp;nid=6.11&amp;next_nid=6.12&amp;cid=9&amp;parent=Security Briefing">Crime</a>
				<a href="content.php?cat=grw&amp;nid=6.12&amp;next_nid=6.13&amp;cid=9&amp;parent=Security Briefing">Money and Valuables</a>
				<a href="content.php?cat=grw&amp;nid=6.13&amp;next_nid=6.14&amp;cid=9&amp;parent=Security Briefing">Transportation Safety</a>
				<a href="content.php?cat=grw&amp;nid=6.14&amp;next_nid=6.15&amp;cid=9&amp;parent=Security Briefing">Local Laws</a>
				<a href="news.php?feed=10&amp;parent=Security Briefing">Crime News Feed</a>
			</div>
		</div>
	</div>


	<div class="accordion-group">
		<div class="accordion-heading">
			<a class="accordion-toggle" data-toggle="collapse" data-parent="#menuCountry" href="#Taxation">
				Taxation
			</a>
		 </div>
		<div id="Taxation" class="accordion-body collapse">
			<div class="accordion-inner">
				<a href="content.php?cat=grw&amp;nid=73.04&amp;next_nid=73.05&amp;cid=9&amp;parent=Taxation">Corporate Taxation</a>
				<a href="content.php?cat=grw&amp;nid=73.05&amp;next_nid=73.06&amp;cid=9&amp;parent=Taxation">Individual Taxation</a>
				<a href="content.php?cat=grw&amp;nid=73.06&amp;next_nid=73.07&amp;cid=9&amp;parent=Taxation">VAT, GST, and Sales Taxes</a>		
			</div>
		</div>
	</div>

	<div class="accordion-group">
		<div class="accordion-heading">
			<a class="accordion-toggle" data-toggle="collapse" data-parent="#menuCountry" href="#Trade">
				Trade
			</a>
		 </div>
		<div id="Trade" class="accordion-body collapse">
			<div class="accordion-inner">			
				<a href="content.php?cat=grw&amp;nid=71&amp;next_nid=72&amp;cid=9&amp;parent=Trade">Trade Profile</a>		
				<a href="content.php?cat=grw&amp;nid=72&amp;next_nid=73&amp;cid=9&amp;parent=Trade">Tariff Profile</a>
				<a href="content.php?cat=grw&amp;nid=61.18&amp;next_nid=61.19&amp;cid=9&amp;parent=Trade">Trade Agreements</a>
				<a href="news.php?feed=7&amp;parent=News Feeds">Trade News Feed</a>						
			</div>
		</div>
	</div>
	

</div>





