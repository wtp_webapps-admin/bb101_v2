
	
	<div class="navbar-header visible-xs visible-sm" style="margin:0px 10px 0px 0px;">
		<div style="display:inline-block; background:black;">
			<p class="nav-right" style="display:inline-block; margin-right:0px; margin-left:10px;">
				<button type="button" class="btn btn-primary btn-sm" data-toggle="offcanvas" style="background:#8694AE;border-color:#000;">Toggle nav</button>
			</p>
			<p class="nav-right" style="display:inline-block; margin-right:5px; margin-left:5px;" >
				<button type="button" class="btn btn-primary btn-sm" data-toggle="search" style="background:#8694AE;border-color:#000;"><span class="glyphicon glyphicon-search"> </span></button>
			</p>
			<p class="nav-right" style="display:inline-block; margin-right:10px; margin-left:0px;">
				<button type="button" class="btn btn-primary btn-sm" data-toggle="translation" style="background:#8694AE;border-color:#000;"><span class="glyphicon glyphicon-flag"> </span></button>
			</p>
		</div>
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
	</div>

	<div class="navbar-collapse collapse">
		<div class="nav navbar-nav navbar-left visible-md visible-lg" style="background:black;margin:0px 10px 0px 0px;">
			<p class="nav-right" style="display:inline-block; margin:10px 0px 10px 10px;" >
				<button type="button" class="btn btn-primary btn-sm" data-toggle="search" style="background:#8694AE;border-color:#000;"><span class="glyphicon glyphicon-search"> </span></button>
			</p>
			<p class="nav-right" style="display:inline-block; margin:10px 10px 10px 0px;">
				<button type="button" class="btn btn-primary btn-sm" data-toggle="translation" style="background:#8694AE;border-color:#000;"><span class="glyphicon glyphicon-flag"> </span></button>
			</p>
		</div>
		<ul class="nav navbar-nav navbar-left" >
			<li class="active" ><a href="#">HOME</a></li>
			<li class="dropdown">
				<a data-toggle="dropdown" class="dropdown-toggle" href="#">ABOUT<span class="caret"></span></a>
				<ul role="menu" class="dropdown-menu">
					<li><a href="#">ABOUT</a></li>
					<li><a href="#">CONTACT</a></li>
					<li><a href="#">CONTRIBUTORS</a></li>
					<li><a href="#">ADMIN</a></li>
				</ul>
			</li>
		</ul>
		<div class="btn-group navbar-right margin-10">
  			<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
				Select another city <span class="caret"></span>
			</button>
			<ul class="dropdown-menu" role="menu">
				<li><a href="193">Aberdeen  (United Kingdom)</a></li>
				<li><a href="75">Abu Dhabi  (UAE)</a></li>
				<li><a href="176">Accra  (Ghana)</a></li>
				<li><a href="109">Addis Ababa  (Ethiopia)</a></li>
				<li><a href="108">Alexandria  (Egypt)</a></li>
				<li><a href="153">Amman  (Jordan)</a></li>
				<li><a href="92">Amsterdam  (Netherlands)</a></li>
				<li><a href="73">Ankara  (Turkey)</a></li>
				<li><a href="194">Antigua  (Antigua and Barbuda)</a></li>
				<li><a href="48">Antwerp  (Belgium)</a></li>
				<li><a href="110">Athens  (Greece)</a></li>
			</ul>
		</div>
		
		<div class="btn-group navbar-right margin-10">
  			<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
				Select another country <span class="caret"></span>
			</button>				
		  	<ul class="dropdown-menu" role="menu">
				<li><a href="1">Afghanistan</a></li>
				<li><a href="2">Albania</a></li>
				<li><a href="3">Algeria</a></li>
				<li><a href="4">Angola</a></li>
				<li><a href="5">Argentina</a></li>
				<li><a href="6">Armenia</a></li>
		  </ul>
		</div>

	</div>
