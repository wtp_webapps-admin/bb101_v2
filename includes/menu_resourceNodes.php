<?php
$webpage = "content.php";
if(isset($_SESSION['subscription']) && $_SESSION['subscription'] == 'grw') {
	$webpage = "content_grw.php";
}
echo "<H4 style='font-size:14px;font-family:Arial;'>World Trade Resources:</H4>
	<div id='my_menu' class='sdmenu'>

		<div class='collapsed'>	 
			<span>Acronyms/Abbreviations</span>	 
			<a href='javascript:goTo(\"acronyms.php\");'>Acronyms and Abbreviations</a>
		</div>	 	

		<div class='collapsed'>	 
			<span>Air Transport</span>	 
			<a href='$webpage?cat=resource&nid=1.1&next_nid=1.2&parent=Air Transport'>Cargo Aircraft</a>
			<a href='$webpage?cat=resource&nid=1.2&next_nid=1.201&parent=Air Transport'>Air Freight Containers</a>
			<a href='javascript:goTo(\"iata.php\");'>World Airport IATA Codes</a>
<!--			<a href='javascript:goTo(\"world_airports_info.php\");'>World Airports Information</a> -->
			<a href='resources.php?cat=world_airports_info.asp'>World Airports Information</a>
			<a href='javascript:goTo(\"airline_codes.php\");'>Airline Codes</a>
		</div> 	

		<div class='collapsed'>	 
			<span>Basics of Int&#39;l Trade</span>	
			<a href='$webpage?cat=resource&nid=3.1&next_nid=3.101&parent=Basics of Intl Trade'>Foreign Exchange</a>
			<a href='$webpage?cat=resource&nid=3.2&next_nid=3.201&parent=Basics of Intl Trade'>Importing</a>
			<a href='$webpage?cat=resource&nid=3.3&next_nid=3.301&parent=Basics of Intl Trade'>Exporting</a>
			<a href='$webpage?cat=resource&nid=3.4&next_nid=3.401&parent=Basics of Intl Trade'>Contracts</a>
		</div> 	
		
		<div class='collapsed'>	 
			<span>Business Entities</span>	 
			<a href='javascript:goTo(\"business_entities.php\");'>Business Entities Worldwide</a>
		</div>	 	

		<div class='collapsed'>	 
			<span>Computer Terms</span>	 
			<a href='javascript:goTo(\"computer_terms.php\");'>Computer Terms</a>
		</div>	 	
		
		<div class='collapsed'>	 
			<span>Country Codes</span>	 
			<a href='resources.php?cat=country_codes.asp'>Country Codes</a>
		</div>	 	

		<div class='collapsed'>	 
			<span>Currencies of the World</span>	 
			<a href='javascript:goTo(\"currency.php\");'>Currencies of the World</a>
		</div>	 	

		<div class='collapsed'>	 
			<span>Dictionary of Int&#39;l Trade</span>	 
			<a href='javascript:goTo(\"a-z.php\");'>A to Z Definitions</a>
		</div>	 	

		<div class='collapsed'>	 
			<span>Exporting from the USA</span>	 
			<a href='$webpage?cat=resource&nid=2.1&next_nid=2.101&parent=Exporting from USA'>Basic Guide to Exporting</a>
			<a href='resources.php?cat=schedule_b_search_new.html'>Export Tariff Codes</a>
			<a href='$webpage?cat=resource&nid=6.01&next_nid=6.021&parent=Exporting from USA'>BIS Regulations</a>
			<a href='javascript:goTo(\"denied_persons.php\");'>Denied Persons List</a>
			<a href='javascript:popUp(\"http://www.pmddtc.state.gov/compliance/debar.html\");'>Debarred List</a>
			<a href='javascript:goTo(\"entity_list.php\");'>Entity List</a>
			<a href='javascript:goTo(\"sdn_list.php\");'>Specially Designated Nationals List</a>
			<a href='javascript:goTo(\"unverifiedlist.php\");'>Unverified List</a>
			<a href='$webpage?cat=resource&nid=3.4&next_nid=3.401&parent=Basics of Intl Trade'>Contracts</a>
		</div> 	

		<div class='collapsed'>	 
			<span>Importing to the USA</span>	 
			<a href='$webpage?cat=resource&nid=5.01&next_nid=5.0101&parent=Importing to USA'>Commodity Index</a>
			<a href='resources.php?cat=hts'>Harmonized Tariff Schedule</a>
			<a href='resources.php?cat=customs_rulings.asp'>US Customs Rulings</a>
			<a href='$webpage?cat=resource&nid=17.1&next_nid=17.1011&parent=Importing to USA'>US Customs Documents</a>
		</div> 	

		<div class='collapsed'>	 
			<span>Incoterms</span>	 
			<a href='$webpage?cat=resource&nid=7.2&next_nid=7.201&parent=Incoterms'>Incoterms 2010</a>
			<a href='$webpage?cat=resource&nid=7.1&next_nid=7.101&parent=Incoterms'>Incoterms 2000</a>
		</div> 	

		<div class='collapsed'>	 
			<span>Insurance, Guide to Cargo</span>
			<a href='$webpage?cat=resource&nid=8.1&next_nid=8.101&parent=Insurance, Guide to Cargo'>Guide to Cargo Insurance</a>
		</div> 	
		
		<div class='collapsed'>	 
			<span>International Dialing</span>	 
			<a href='$webpage?cat=resource&nid=9.1&next_nid=9.101&parent=International Dialing'>International Dialing Guide</a>
			<a href='javascript:goTo(\"intl_dialing_codes.php\");'>International Dialing Codes</a>
		</div> 	
		
		<div class='collapsed'>	 
			<span>International Payments</span>	 
			<a href='$webpage?cat=resource&nid=10.01&next_nid=10.0101&parent=International Payments'>Documentary Collections</a>
			<a href='$webpage?cat=resource&nid=10.201&next_nid=10.202&parent=International Payments'>Letters of Credit</a>
		</div> 	
		
		<div class='collapsed'>	 
			<span>Measurement Converters</span>	 
			<a href='javascript:goTo(\"18conversiontoolsbody.php\");'>Measurement Converters</a>
		</div> 	
		
		<div class='collapsed'>	 
			<span>NAFTA</span>	 
			<a href='$webpage?cat=NAFTA&page=a&parent=NAFTA'>Overview</a>
			<a href='$webpage?cat=NAFTA&page=b&parent=NAFTA'>Text</a>
			<a href='$webpage?cat=NAFTA&page=c&parent=NAFTA'>Implementation</a>
			<a href='$webpage?cat=NAFTA&page=d&parent=NAFTA'>Documentation</a>
<!--			<a href='$webpage?cat=NAFTA&page=Resources.html&parent=NAFTA'>Resources</a> -->
		</div> 	
		
		<div class='collapsed'>	 
			<span>Ocean Transport</span>	 
			<a href='$webpage?cat=resource&nid=12.01&next_nid=12.02&parent=Ocean Transport'>Cargo Vessels</a>
			<a href='$webpage?cat=resource&nid=12.02&next_nid=12.03&parent=Ocean Transport'>Cranes</a>
			<a href='$webpage?cat=resource&nid=12.03&next_nid=12.04&parent=Ocean Transport'>Ocean Freight Containers</a>
			<a href='resources.php?cat=world_ports.asp'>Seaports of the World</a>
			<a href='javascript:goTo(\"ports_distances.php\");'>Distances Between Ports</a>
			<a href='$webpage?cat=resource&nid=12.6&next_nid=12.601&parent=Ocean Transport'>Vessel Classification</a>
		</div> 	

		<div class='collapsed'>	 
			<span>Railcars</span>	 
			<a href='$webpage?cat=resource&nid=13.01&next_nid=13.0101&parent=Railcars'>Guide to Railcars</a>
		</div> 	
		
		<div class='collapsed'>	 
			<span>Resources for Int&#39;l Trade</span>	 
			<a href='$webpage?cat=resource&nid=14.01&next_nid=14.02&parent=Resources for Intl Trade'>Books and Directories</a>
			<a href='$webpage?cat=resource&nid=14.02&next_nid=14.03&parent=Resources for Intl Trade'>ICC Publications</a>
			<a href='$webpage?cat=resource&nid=14.03&next_nid=14.04&parent=Resources for Intl Trade'>Periodicals and Reports</a>
			<a href='$webpage?cat=resource&nid=14.04&next_nid=14.05&parent=Resources for Intl Trade'>Country Series Books</a>
			<a href='$webpage?cat=resource&nid=14.05&next_nid=14.06&parent=Resources for Intl Trade'>Trade Associations</a>
			<a href='$webpage?cat=resource&nid=14.06&next_nid=14.07&parent=Resources for Intl Trade'>Academic Institutions</a>
			<a href='$webpage?cat=resource&nid=14.07&next_nid=14.08&parent=Resources for Intl Trade'>Travel Websites</a>
			<a href='$webpage?cat=resource&nid=14.08&next_nid=14.09&parent=Resources for Intl Trade'>Trade-Related Websites</a>
			<a href='$webpage?cat=resource&nid=14.09&next_nid=14.1&parent=Resources for Intl Trade'>Other Information Sources</a>

		</div> 	

		<div class='collapsed'>	 
			<span>Security</span>	 
			<a href='images/DITsupplychain.pdf'>Supply Chain Illustration</a>
			<a href='$webpage?cat=resource&nid=15.02&next_nid=15.03&parent=Security'>C-TPAT</a>
			<a href='$webpage?cat=resource&nid=15.03&next_nid=15.04&parent=Security'>FAST</a>
			<a href='$webpage?cat=resource&nid=15.04&next_nid=15.05&parent=Security'>ACE</a>
			<a href='$webpage?cat=resource&nid=15.05&next_nid=15.06&parent=Security'>PAPS</a>
			<a href='$webpage?cat=resource&nid=15.06&next_nid=15.07&parent=Security'>PARS</a>
			<a href='$webpage?cat=resource&nid=15.07&next_nid=15.08&parent=Security'>C-TPAT Seal Requirements</a>
			<a href='$webpage?cat=resource&nid=15.08&next_nid=15.09&parent=Security'>Automated Manifest System</a>
			<a href='$webpage?cat=resource&nid=15.09&next_nid=15.1&parent=Security'>CSI</a>
			<a href='$webpage?cat=resource&nid=15.1&next_nid=15.11&parent=Security'>AMR (24-Hour Rule)</a>
			<a href='$webpage?cat=resource&nid=15.11&next_nid=15.12&parent=Security'>Food Facility Registration</a>
			<a href='$webpage?cat=resource&nid=15.12&next_nid=15.13&parent=Security'>ISPS Code</a>
			<a href='$webpage?cat=resource&nid=15.13&next_nid=15.14&parent=Security'>Maritime Transportation Security Act</a>
			<a href='$webpage?cat=resource&nid=15.14&next_nid=15.15&parent=Security'>E.U. Maritime Legislation</a>
			<a href='$webpage?cat=resource&nid=15.15&next_nid=15.16&parent=Security'>10+2 Rule</a>
			<a href='$webpage?cat=resource&nid=15.16&next_nid=15.17&parent=Security'>ISO 28000</a>
			<a href='$webpage?cat=resource&nid=15.17&next_nid=15.18&parent=Security'>Data Security</a>
			<a href='$webpage?cat=resource&nid=15.18&next_nid=15.19&parent=Security'>Industrial Espionage</a>
			<a href='resources.php?cat=glossary_security.asp'>Security Glossary</a>
		</div> 	

		<div class='collapsed'>	 
			<span>Sourcing Guide</span>	 
			<a href='$webpage?cat=resource&nid=18.01&next_nid=18.0101&parent=Sourcing Guide'>Sourcing Guide</a>
		</div> 	

		<div class='collapsed'>	 
			<span>Terms in 8 Languages</span>	 
			<a href='javascript:goTo(\"keywords.php\");'>Trade Terms in 8 Languages</a>
		</div> 	

		<div class='collapsed'>	 
			<span>Truck Trailers</span>	 
			<a href='$webpage?cat=resource&nid=16.01&next_nid=16.0101&parent=Truck Trailers'>Guide to Truck Trailers</a>
		</div> 	



		<div class='collapsed'>	 
			<span>Weights and Measures</span>	 	
			<a href='$webpage?cat=resource&nid=19.01&next_nid=19.0101&parent=Weights and Measures'>Weights and Measures</a>
		</div> 	
		
	</div>";
?>


