<?PHP
$webpage = "content.php";
$cid = $_SESSION['cid'];
//city_id = Session("city_id")
//city_name = Session("city_name")
if($_SESSION['subscription'] == 'grw') 	$webpage = "content_grw.php";
if($_SESSION['session_name'] == '_cbi') 	$webpage = "content_cbi.php";
echo "

	<br style='clear:both' />
 <div id='my_menu' class='sdmenu'>


	
	

	<div class='collapsed'>
	  <span>Overviews</span>
	  <a href='$webpage?nid=1&next_nid=2&city_id=$city_id&cat=city'>City View</a>	
<!--	  <a href='city_facts.asp?city_id=$city_id'>City Facts</a>	-->
	  <a href='$webpage?m=cities&nid=3&next_nid=4&city_id=$city_id&cat=city'>Neighborhoods</a>		
	  <a href='$webpage?m=cities&nid=30&next_nid=31&city_id=$city_id&cat=city'>Insider&#39;s Guide</a>		
	  <a href='$webpage?m=cities&nid=62&next_nid=63&city_id=$city_id&cat=city'>City Summary</a>		
	</div>
	
	<div class='collapsed'>
	  <span>Activities</span>
	  <a href='$webpage?m=cities&nid=4&next_nid=5&city_id=$city_id&cat=city'>Attractions</a>		
	  <a href='$webpage?m=cities&nid=5&next_nid=6&city_id=$city_id&cat=city'>Excursions</a>		
	  <a href='$webpage?m=cities&nid=6&next_nid=7&city_id=$city_id&cat=city'>Nightlife</a>		
	  <a href='$webpage?m=cities&nid=7&next_nid=8&city_id=$city_id&cat=city'>Shopping</a>			
	</div>	

	<div class='collapsed'>
	  <span>Food and Restaurants</span>
	  <a href='$webpage?m=cities&nid=20.14&next_nid=20.15&cid=$cid&cat=grw&city_id=$city_id'>National Cuisine</a>	
	  <a href='$webpage?m=cities&nid=20.37&next_nid=20.38&cid=$cid&cat=grw&city_id=$city_id'>Recipes</a>	
	  <a href='$webpage?m=cities&nid=8&next_nid=9&city_id=$city_id&cat=city'>Restaurants</a>		
	</div>	

	
	<div class='collapsed'>
	  <span>Climate and Weather</span>
	  <a href='$webpage?m=cities&nid=9&next_nid=10&city_id=$city_id&cat=city'>Climate Summary</a>				
	 <!-- <a href='$webpage?m=cities&nid=63&next_nid=64&city_id=$city_id&cat=grw'>National Weather</a>				 -->
	</div>	

	
	<div class='collapsed'>
	  <span>Embassies/Consulates</span>
	  <a href='$webpage?m=cities&nid=embassies&cid=$cid&cat=grw&city_id=$city_id'>Embassies and Consulates</a>
	</div>

	
	<div class='collapsed'>
	  <span>Health and Medical</span>
	  <a href='$webpage?m=cities&nid=4.02&next_nid=4.03&cid=$cid&cat=grw&city_id=$city_id'>Immunization Summary</a>
	  <a href='$webpage?m=cities&nid=7.05&next_nid=7.06&cid=$cid&cat=grw&city_id=$city_id'>Disease Risks and Prevention</a>		
	  <a href='$webpage?m=cities&nid=7.06&next_nid=7.07&cid=$cid&cat=grw&city_id=$city_id'>Health Care System</a>	
	  <a href='$webpage?m=cities&nid=7.08&next_nid=7.09&cid=$cid&cat=grw&city_id=$city_id'>Insurance and Med-Evac</a>	
	  <a href='$webpage?m=cities&nid=15.06&next_nid=15.07&cid=$cid&cat=grw&city_id=$city_id'>Women&#39;s Health Issues</a>
	  <a href='$webpage?m=cities&nid=7.07&next_nid=7.08&city_id=$city_id&cat=grw'>Health Advisories</a>
	</div>

	
	
	
	<div class='collapsed'>
	  <span>Language</span>
	  <a href='$webpage?m=cities&nid=16.0&next_nid=17&cid=$cid&cat=grw&city_id=$city_id'>Essential Terms</a>
	</div>

<!--	
	<div class='collapsed'>
	  <span>Maps</span>
	  <a href='map1.asp?nid=23&next_nid=24&city_id=$city_id&cat=downtown'>Downtown Map</a>
	  <a href='map1.asp?nid=23&next_nid=24&city_id=$city_id&cat=city'>City Map</a>
	  <a href='map1.asp?nid=23&next_nid=24&city_id=$city_id&cat=metro'>Metro Area Map</a>
	  <a href='map1.asp?nid=23&next_nid=24&city_id=$city_id&cat=airport'>Airport Map</a>
	  <a href='map1.asp?nid=23&next_nid=24&city_id=$city_id&cat=region'>Regional Map</a>	
	</div>
-->
	


	
	<div class='collapsed'>
	  <span>Travel Essentials</span>
	  <a href='$webpage?m=cities&nid=6.05&next_nid=6.06&cid=$cid&cat=grw&city_id=$city_id'>Travel Warnings</a>	
	  <a href='$webpage?m=cities&nid=4.01&next_nid=4.02&cid=$cid&cat=grw&city_id=$city_id&parent=Travel Essentials'>Passport and Visa</a>
	  <a href='$webpage?m=cities&nid=4.03&next_nid=4.04&cid=$cid&cat=grw&city_id=$city_id&parent=Travel Essentials'>Customs Entry (Personal)</a>
	  <a href='$webpage?m=cities&nid=4.04&next_nid=4.05&cid=$cid&cat=grw&city_id=$city_id&parent=Travel Essentials'>Departure Formalities</a>
	</div>	

	<div class='collapsed'>
	  <span>Transportation</span>
	  <a href='$webpage?m=cities&nid=11&next_nid=12&city_id=$city_id&cat=city'>Airports</a>			
	  <a href='$webpage?m=cities&nid=13&next_nid=14&city_id=$city_id&cat=city'>Autos</a>			
	  <a href='$webpage?m=cities&nid=15&next_nid=16&city_id=$city_id&cat=city'>Ferries and Boats</a>				
	  <a href='$webpage?m=cities&nid=17&next_nid=18&city_id=$city_id&cat=city'>Public Transportation</a>				
	  <a href='$webpage?m=cities&nid=14&next_nid=15&city_id=$city_id&cat=city'>Taxis</a>			
	  <a href='$webpage?m=cities&nid=16&next_nid=17&city_id=$city_id&cat=city'>Trains</a>	
	  <a href='$webpage?m=cities&nid=63&next_nid=64&city_id=$city_id&cat=city'>Travel Routes&#151;Domestic</a>
	  <a href='$webpage?m=cities&nid=65&next_nid=66&city_id=$city_id&cat=city'>Travel Routes&#151;International</a>	
	</div>
	
	
	<div class='collapsed'>
	  <span>Money and Banking</span>
	  <a href='$webpage?m=cities&nid=3.01&next_nid=3.02&cid=$cid&cat=grw&city_id=$city_id&parent=Money and Banking'>Currency</a>	
	  <a href='$webpage?m=cities&nid=3.02&next_nid=3.03&cid=$cid&cat=grw&city_id=$city_id&parent=Money and Banking'>Banknotes</a>
	  <a href='$webpage?m=cities&nid=3.08&next_nid=3.09&cid=$cid&cat=grw&city_id=$city_id&parent=Money and Banking'>Coins</a>
	  <a href='http://globalroadwarrior.com/currency_converter_php/currency.php?cid=$cid' onClick=\"window.open('http://globalroadwarrior.com/currency_converter_php/currency.php?cid=$cid', 'Currency Converter', 'height=550,width=810,scrollbars=yes,resizable=yes'); return false;\" target=\"_blank\" >Currency Converter</a>	
	</div>	
	
	<div class='collapsed'>
	  <span>Communications</span>
	  <a href='$webpage?m=cities&nid=9.01&next_nid=9.02&cid=$cid&cat=grw&city_id=$city_id&parent=Communications'>Quick Start</a>	
	  <a href='$webpage?m=cities&nid=9.02&next_nid=9.03&cid=$cid&cat=grw&city_id=$city_id&parent=Communications'>Dialing Guide</a>	
	  <a href='$webpage?m=cities&nid=4.0602&next_nid=4.0603&cid=$cid&cat=grw&city_id=$city_id&parent=Travel Essentials'>Emergency Numbers</a>
	  <a href='$webpage?m=cities&nid=9.05&next_nid=9.06&cid=$cid&cat=grw&city_id=$city_id&parent=Communications'>Notable Calling Features</a>
	  <a href='$webpage?m=cities&nid=9.07&next_nid=9.08&cid=$cid&cat=grw&city_id=$city_id&parent=Communications'>Mobile Phones</a>
	  <a href='$webpage?m=cities&nid=9.08&next_nid=9.09&cid=$cid&cat=grw&city_id=$city_id&parent=Communications'>Traveling Mobile Phones</a>
	</div>		

	<div class='collapsed'>
	  <span>Electrical</span>
	  <a href='$webpage?m=cities&nid=12&next_nid=13&cid=$cid&cat=grw&city_id=$city_id&parent=Electrical'>Electrical</a>	
	</div>		


	  </div>"

?>
