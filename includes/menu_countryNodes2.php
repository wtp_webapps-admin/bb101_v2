<?php
error_reporting(0);
?>

	<div class="panel panel-default" id="menuCountry">
		
		<H4>[country name]</H4>
		
		<div class="panel-heading">
			<h4 class="panel-title"><a data-toggle="collapse" data-parent="#menuCountry" href="#overviews">Overviews</a></h4>
		</div>
		<div id="overviews" class="panel-collapse collapse">
			<div class="accordion-inner">
				<a href="content.php?cat=grw&amp;nid=66&amp;next_nid=67&amp;cid=9&amp;parent=Overviews">Trade Overview</a>
				<a href="content.php?cat=grw&amp;nid=67&amp;next_nid=68&amp;cid=9&amp;parent=Overviews">Business Overview</a>
				<a href="content.php?cat=grw&amp;nid=65&amp;next_nid=66&amp;cid=9&amp;parent=Overviews">Country Snapshot</a>
				<a href="country_facts.php?cid=9&amp;parent=Overview">Country Facts</a>
			</div>
		</div>

		<div class="panel-heading">
			<h4 class="panel-title"><a data-toggle="collapse" data-parent="#menuCountry" href="#bizCulture">Business Culture</a></h4>
		</div>
		<div id="bizCulture" class="panel-collapse collapse">
			<div class="accordion-inner">
				<a href="content.php?cat=grw&amp;nid=13.12&amp;next_nid=13.13&amp;cid=9&amp;parent=Business Culture">The Business Experience</a>
				<a href="content.php?cat=grw&amp;nid=13.04&amp;next_nid=13.05&amp;cid=9&amp;parent=Business Culture">Decision Making</a>
				<a href="content.php?cat=grw&amp;nid=13.11&amp;next_nid=13.12&amp;cid=9&amp;parent=Business Culture">Meetings</a>
				<a href="content.php?cat=grw&amp;nid=13.10&amp;next_nid=13.11&amp;cid=9&amp;parent=Business Culture">Negotiating</a>
			</div>
		</div>
	</div>






